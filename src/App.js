import React from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends React.Component {
  state={
    userInput: "No data provided",
  };

  handleChange=(event)=> {
    let data = event.target.value;
    console.log(data);
    data.length === 0
      ? this.setState({ userInput: "No data provided" })
      : this.setState({ userInput: data });
  };
  render(){
  return (
    <div className="App">
      <input onChange={this.handleChange} />
        <h1>{this.state.userInput}</h1>
    </div>
  );
}
}
export default App;
